$( document ).ready(function() {

  	$(window).bind('scroll', function () {
	    if ($(window).scrollTop() > 60 && $(window).width() > 768) {
	        $('header').addClass('fixed');
	        $('main__title__wrap').addClass('mt');
	    } else {
	        $('header').removeClass('fixed');
	        $('main__title__wrap').addClass('mt');
	    }
	});

	function firstScreen() {
		if ($(window).height() < 930 && $(window).width() > 1023) {
			$('.first__screen').addClass('centered');
			$('.first__screen').addClass('medium');
		} else {
			$('.first__screen').removeClass('centered');
		};

		if ($(window).height() < 860 && $(window).width() > 1023) {
			$('.first__screen').removeClass('medium');
			$('.first__screen').addClass('small');
		};

		if ($(window).width() < 1024) {
			$('.first__screen').removeClass('medium');
			$('.first__screen').removeClass('small');
			$('.first__screen').removeClass('centered');
		};
	};

	firstScreen();

	$(window).on('resize', function(){
		firstScreen();
	});

	$(".choose__block__item__content, .popup, .conf__policy__popup").overlayScrollbars({});

	$('.choose__block__item__heading').click(function(){
		if ($(window).width() < 769) {
			if ($(this).parents('.choose__block__item').hasClass('open')) {

				$(this).parents('.choose__block__item').removeClass('open');
				$(this).siblings('.choose__block__item__content__wrap').children('.choose__block__item__content').slideUp();

			} else {
				$(this).parents('.choose__block__item').addClass('open');
				$(this).siblings('.choose__block__item__content__wrap').children('.choose__block__item__content').slideDown();
			}
		};
	});

	// $(window).resize(function() {
	// 	firstScreen();
	// });

	var slick1 = $('.success__block__slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		adaptiveHeight: true
	});

	var slick2 = $('.partners__block__slider').slick({
		slidesToShow: 7,
		slidesToScroll: 1,
		arrows: true,
  		autoplay: true,
  		autoplaySpeed: 2400,
  		pauseOnHover: false,
  		pauseOnFocus:false,
		responsive: [
	    {
	      breakpoint: 1199,
	      settings: {
	        slidesToShow: 6,
	        slidesToScroll: 1
	      }
	    },
	     {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 5,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 860,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 767,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 576,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});

	Tablesaw.init();

	(function lpSwitch() {

		if ($('.lp__switch').data("lp") === "usergate") {
			$('body').attr("data-switch","usergate");
			$('body [data-switch="kerio"]').hide();
			$('body [data-switch="usergate"]').fadeIn();
			$('.lp__switch').attr("data-lp","usergate");
			$('.lp__switch__title').text('Выбрать Kerio');
			$('table').trigger( "enhance.tablesaw" );
			Tablesaw.init();

		} else if ($('.lp__switch').data("lp") === "kerio") {
			$('body').attr("data-switch","kerio");
			$('body [data-switch="kerio"]').fadeIn();
			$('body [data-switch="usergate"]').hide();
			$('.lp__switch').attr("data-lp","kerio");
			$('.lp__switch__title').text('Выбрать UserGate');
			$('table').trigger( "enhance.tablesaw" );
			Tablesaw.init();
		}

		slick1.slick('refresh');
		slick2.slick('refresh');

	}());

	(function () {
    	var activeTab = $('.tabs__switch.active').data('tab');

		$('.tabs__wrap .tab').not('[data-tab="'+activeTab+'"]').hide();
		$('.tabs__wrap .tab[data-tab="'+activeTab+'"]').fadeIn();
	}());

	$('.tabs__switch').click(function(){
		var dataTab = $(this).data('tab');

		$('.tabs__switch').not('[data-tab="'+dataTab+'"]').removeClass('active');
		$('.tabs__switch[data-tab="'+dataTab+'"]').addClass('active');
		$('.tabs__wrap .tab').not('[data-tab="'+dataTab+'"]').hide();
		$('.tabs__wrap .tab[data-tab="'+dataTab+'"]').fadeIn();
	});

	$('.accordion__heading').click(function(){
		if ($(this).parents('.accordion').hasClass('open')) {
			$(this).parents('.accordion').removeClass('open');
			$(this).siblings('.accordion__content').slideUp();
		} else {
			$(this).parents('.accordion').addClass('open');
			$(this).siblings('.accordion__content').slideDown();
		}
	});

	$('.choose__block__item .btn__show__more').click(function(){

		if ($(this).attr('data-lp') === "usergate") {
			$('body').attr("data-switch","usergate");
			$('body [data-switch="kerio"]').hide();
			$('body [data-switch="usergate"]').fadeIn();
			$('.lp__switch').attr("data-lp","usergate");
			$('.lp__switch__title').text('Выбрать Kerio');
			$('table').trigger( "enhance.tablesaw" );
			Tablesaw.init();

		} else if ($(this).attr('data-lp') === "kerio") {
			$('body').attr("data-switch","kerio");
			$('body [data-switch="kerio"]').fadeIn();
			$('body [data-switch="usergate"]').hide();
			$('.lp__switch').attr("data-lp","kerio");
			$('.lp__switch__title').text('Выбрать UserGate');
			$('table').trigger( "enhance.tablesaw" );
			Tablesaw.init();
		}

		slick1.slick('refresh');
		slick2.slick('refresh');

		$('.first__screen').addClass('hidden');
		$('html').removeClass('overflow-hidden');

		if ($(window).width() < 1200) {
			$('html, body').animate({
			      scrollTop: $('header').offset().top - 5
			  }, 50);
			  return false;
		}
	});

	$('.lp__switch').click(function(){

		$('html, body').animate({
	      scrollTop: $('body').offset().top
	  	}, 0);

		if ($(this).attr('data-lp') === "usergate") {

			$(this).attr("data-lp","kerio");
			$('body').attr("data-switch","kerio");
			$('body [data-switch="kerio"]').fadeIn();
			$('body [data-switch="usergate"]').hide();
			$('.lp__switch__title').text('Выбрать UserGate');
			$('table').trigger( "enhance.tablesaw" );
			Tablesaw.init();

		} else if ($(this).attr('data-lp') === "kerio") {

			$(this).attr("data-lp","usergate");
			$('body').attr("data-switch","usergate");
			$('body [data-switch="kerio"]').hide();
			$('body [data-switch="usergate"]').fadeIn();
			$('.lp__switch__title').text('Выбрать Kerio');
			$('table').trigger( "enhance.tablesaw" );
			Tablesaw.init();
		};

		slick1.slick('refresh');
		slick2.slick('refresh');
	});

	$('.get__order__usergate__test__popup').click(function() {
		$('.overlay').fadeIn();
		$('.order__usergate__test__popup').fadeIn();
	});

	$('.get__order__kerio__test__popup').click(function() {
		$('.overlay').fadeIn();
		$('.order__kerio__test__popup').fadeIn();
	});

	$('.get__callback__popup').click(function() {
		$('.overlay').fadeIn();
		$('.callback__popup').fadeIn();
	});

	$('.get__conf__policy__popup').click(function() {
		$('.overlay').fadeIn();
		$('.conf__policy__popup').fadeIn();

		if ($(this).hasClass('custom')) {
			$('.conf__policy__popup').addClass('custom');
		} else {
			$('.conf__policy__popup').removeClass('custom');
		}
	});

	$('.get__consult__btn').click(function() {
		var dataItem = $(this).data('item');

		$('.overlay').fadeIn();
		$('.leave__request__popup').fadeIn();

		$('.leave__request__popup button').attr('data-item', dataItem);
	});

	$('.overlay').click(function() {
		$('.overlay').fadeOut();
		$('.popup, .popup__reminder, .conf__policy__popup').fadeOut();
	});

	$('.popup .popup__close, .popup__reminder .popup__close').click(function() {
		$('.overlay').fadeOut();
		$('.popup, .popup__reminder').fadeOut();
	});

	$('.conf__policy__popup .popup__close').click(function() {

		$('.conf__policy__popup').fadeOut();

		if ($('.conf__policy__popup').hasClass('custom')) {
			$('.overlay').fadeOut();
		}
	});

	$(document).on("click",".features__block__switch",function() {
		if ($(window).width() < 1024) {

			if ($(this).parents('.container').attr('data-switch') === "usergate") {

				  $("html, body").animate({
				  	scrollTop: $('.container[data-switch="usergate"] .features__block__tabs__wrap').offset().top - ($(window).height()-$('.container[data-switch="usergate"] .features__block__tabs__wrap').outerHeight() - 30)
				  }, 500);
			}

			if ($(this).parents('.container').attr('data-switch') === "kerio") {
				  $("html, body").animate({
				  	scrollTop: $('.container[data-switch="kerio"] .features__block__tabs__wrap').offset().top - ($(window).height()-$('.container[data-switch="kerio"] .features__block__tabs__wrap').outerHeight() - 30)
				  }, 500);
			}
		}
	});

	$('.mob__menu__btn').click(function() {

		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			$('.header__menu__wrap').removeClass('active');
		} else {
			$(this).addClass('active');
			$('.header__menu__wrap').addClass('active');
		}
	});

	$('.header__menu__item').click(function() {

		if ($(window).width() < 1024) {
			$('.mob__menu__btn').removeClass('active');
			$('.header__menu__wrap').removeClass('active');

		  $('html, body').animate({
		      scrollTop: $($.attr(this, 'href')).offset().top + 10 
		  }, 500);
		  return false;
		} else {
		  $('html, body').animate({
		      scrollTop: $($.attr(this, 'href')).offset().top - 100 
		  }, 500);
		  return false;
		}

		
	});

	$('input[type="tel"]').inputmask('+7 (999) 999-99-99');
});