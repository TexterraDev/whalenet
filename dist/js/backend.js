$( document ).ready(function() {
	$('.leave__request__form button[type="submit"]').click(function(event) {
		event.preventDefault();
		
		$('.leave__request__form').hide();
		$('.leave__request__reminder').fadeIn();
	});

	$('.leave__request__popup button[type="submit"]').click(function(event) {
		event.preventDefault();

		$('.leave__request__popup').fadeOut();
		$('.popup__reminder').fadeIn();
	});

	$('.order__test__popup button[type="submit"]').click(function(event) {
		event.preventDefault();

		$('.order__test__popup').fadeOut();
		$('.popup__reminder').fadeIn();
	});

	$('.callback__popup button[type="submit"]').click(function(event) {
		event.preventDefault();

		$('.callback__popup').fadeOut();
		$('.popup__reminder').fadeIn();
	});
});